package org.gtug.collage;

import org.gtug.classes.Collage;
import org.gtug.classes.ImageObject;
import org.gtug.classes.TextObject;
import org.gtug.collage.fragments.CollageCreationFragment;
import org.gtug.collage.fragments.FonPickerFragment;
import org.gtug.collage.fragments.InterfaceFragment;
import org.gtug.collage.fragments.TextEditFragment;
import org.gtug.ui.CollageLayout;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

public class CollageCreationActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		startFonPickerFragment();
	}
	
	protected void startFonPickerFragment(){
		FonPickerFragment fonPickerFragment = FonPickerFragment.newInstance();
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.add(android.R.id.content, fonPickerFragment).commit();
	}
	
	public void startCreationWithBackground(int background){
		startCollageCreationFragment(background);
		showInterface();
	}
	
	private void startCollageCreationFragment(int background){
		collageCreationFragment = CollageCreationFragment.newInstance(background);
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.replace(android.R.id.content, collageCreationFragment).commit();
	}
	
	public void startTextEditFragment(){
		TextEditFragment textEditFragment = TextEditFragment.newInstance();
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.addToBackStack(null);
		ft.add(android.R.id.content, textEditFragment).commit();
	}
	
	public void addText(int background,String text,int textColor, int textSize){
		Bundle arguments = new Bundle();
		arguments.putInt("bg", background);
		arguments.putString("text", text);
		arguments.putInt("textcolor", textColor);
		arguments.putInt("textsize", textSize);
		collageCreationFragment.addText(arguments);
	}
	
	public void choosePicture() {
		collageCreationFragment.choosePicture();
	}
	
	public void saveCollage(){
		collageCreationFragment.save();
		finishCreation();
	}
	
	public void finishCreation(){
		setResult(RESULT_OK);
		finish();
	}
	
	public void showInterface(){
		interfaceFragment = InterfaceFragment.newInstance();
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.addToBackStack(null);
		ft.add(android.R.id.content, interfaceFragment).commit();
	}
	
	public void hideInterface(){
		interfaceFragment.getFragmentManager().popBackStack();
	}
	
	public void onInterfaceButtonClicked(){
		collageCreationFragment.hideInterface();
	}
	
	CollageCreationFragment collageCreationFragment;
	InterfaceFragment interfaceFragment;
}
