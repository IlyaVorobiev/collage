package org.gtug.collage;

import java.util.ArrayList;

import org.gtug.adapters.CollageViewAdapter;
import org.gtug.classes.Collage;
import org.gtug.database.Provider;

import android.app.Activity;
import android.app.WallpaperManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class StartActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        initViews();
    }
    
    private void initViews(){
    	newCollage = (ImageButton) findViewById(R.id.create);
    	bg = (RelativeLayout) findViewById(R.id.collage);
    	ImageButton setAsWallpapers = (ImageButton) findViewById(R.id.setwallpaper);
    	ImageButton remove = (ImageButton) findViewById(R.id.remove);
    	initCollagesList();
    	
    	setAsWallpapers.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				setAsWallpapers();
			}
		});
    	
    	newCollage.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				createNewCollage();
			}
		});
    	
    	remove.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				removeCurrentCollage();
			}
		});
    }
    
    private void initCollagesList() {
		mProvider = new Provider(this);
		allCollages = mProvider.getAllColages();
		colAdapter = new CollageViewAdapter(allCollages, this);
		ViewPager pages = (ViewPager)findViewById(R.id.collageView);
		pages.setAdapter(colAdapter);
	}
    
    private void setAsWallpapers(){
    	ViewPager pages = (ViewPager)findViewById(R.id.collageView);
    	WallpaperManager manager = WallpaperManager.getInstance(this);
		View currentView = pages.getChildAt(pages.getCurrentItem());
		currentView.setDrawingCacheEnabled(true);
		Bitmap wallpapers = currentView.getDrawingCache();
		try {
			   manager.setBitmap(currentView.getDrawingCache());
			   Toast toast = Toast.makeText(this, "Wallpapers has been set!", Toast.LENGTH_SHORT);
			   toast.show();
		} catch (Exception e) {
			   Log.d("collage","error");
		       e.printStackTrace();
		}
    }
    
    private void removeCurrentCollage() {
    	ViewPager pages = (ViewPager)findViewById(R.id.collageView);
    	Log.d("collage",Integer.toString(pages.getCurrentItem()));
    	pages.removeViewAt(0);
    	mProvider.removeCollage(colAdapter.allCollages.get(allCollages.size() - pages.getCurrentItem() -1).id);
    	CollageViewAdapter a = (CollageViewAdapter) pages.getAdapter();
    	try{
    		a.allCollages.remove(allCollages.size() - pages.getCurrentItem() -1);
    		a.notifyDataSetChanged();
    	} catch(Exception e){
    		
    	}
    	pages.setCurrentItem(0);
    	
//    	a.notifyDataSetChanged();
//    	allCollages = mProvider.getAllColages();
//    	colAdapter.allCollages = allCollages;
//    	colAdapter.notifyDataSetChanged();
//    	pages.setAdapter(colAdapter);
	}
    
    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == RESULT_OK){
			allCollages = mProvider.getAllColages();
			colAdapter.allCollages = allCollages;
			ViewPager pages = (ViewPager)findViewById(R.id.collageView);
			pages.setAdapter(colAdapter);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		for(Collage collage: allCollages) {
			collage.stop();
		}
	}


	private void createNewCollage() {
    	Intent collageCreationIntent = new Intent(this,CollageCreationActivity.class);
    	startActivityForResult(collageCreationIntent,1);
    }
    
    private ImageButton newCollage;
    private RelativeLayout bg;
    private Provider mProvider;
    private CollageViewAdapter colAdapter;
    private boolean wasAdapterInitialized = false;
    ArrayList<Collage> allCollages;
}