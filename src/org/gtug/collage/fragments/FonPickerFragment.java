package org.gtug.collage.fragments;

import org.gtug.adapters.FonPickerAdapter;
import org.gtug.collage.CollageCreationActivity;
import org.gtug.collage.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

public class FonPickerFragment extends Fragment {
	
	public static FonPickerFragment newInstance(){
		FonPickerFragment instance = new FonPickerFragment();
		return instance;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		backgrounds.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View v, int position,
					long arg3) {
				((CollageCreationActivity)getActivity()).startCreationWithBackground(((FonPickerAdapter)backgrounds.getAdapter()).backgrounds[position].fullSize);
			}
		});
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.fonpicker, container,false);
		backgrounds = (GridView) v.findViewById(R.id.backgrounds);
		final FonPickerAdapter a = new FonPickerAdapter(getActivity());
		backgrounds.setAdapter(a);
		
		return v;
	}
	
	GridView backgrounds;
}
