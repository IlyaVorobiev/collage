package org.gtug.collage.fragments;
import org.gtug.adapters.ColorPickerAdapter;
import org.gtug.adapters.FrameSelectAdapter;
import org.gtug.adapters.SizePickerAdapter;
import org.gtug.classes.ColorPickerDialog;
import org.gtug.classes.ColorPickerDialog.OnColorChangedListener;
import org.gtug.collage.CollageCreationActivity;
import org.gtug.collage.R;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;


public class TextEditFragment extends Fragment {
	
	public static TextEditFragment newInstance(){
		TextEditFragment textEditFragment = new TextEditFragment();
		return textEditFragment;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initViewSettings();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.textedit, container,false);
		initViews(v);
		return v;
	}
	
	private void initViews(View v) {
		text = (EditText)v.findViewById(R.id.text);
	    framesList = (ListView)v.findViewById(R.id.frameslist);
	    sizeSelector = (Spinner)v.findViewById(R.id.textsize);
	    colorPicker = (ImageView)v.findViewById(R.id.color);
		textColor = text.getTextColors().getDefaultColor();
		colorPicker.setBackgroundColor(textColor);
		saveButton = (ImageButton)v.findViewById(R.id.saveText);
		RelativeLayout background = (RelativeLayout)v.findViewById(R.id.main);
		background.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
			}
		});
	}
	
	private void initViewSettings(){
		setFramesListSettings();
		setSizeSelectorSettings();
		setColorPickerSettings();
		setSaveButtonSettings();
	}
	
	private void setFramesListSettings(){
		final FrameSelectAdapter adapter = new FrameSelectAdapter(getActivity());
		framesList.setAdapter(adapter);
		framesList.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View v, int position,
					long arg3) {
				background = adapter.frames[position].picture;
				text.setBackgroundResource(background);
			}
		});
	}
	
	private void setSizeSelectorSettings(){
		sizeSelector.setAdapter(new SizePickerAdapter(getActivity()));
		sizeSelector.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> parent, View v,
					int position, long arg3) {
				textSize = SizePickerAdapter.sizes[position];
				text.setTextSize(textSize);
				
			}

			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
		});
	}
	
	private void setColorPickerSettings(){
		colorPicker.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				startPickColor();
			}
		});
	}
	
	private void startPickColor(){
		final ColorPickerDialog colors = new ColorPickerDialog(getActivity(), new OnColorChangedListener() {
			
			@Override
			public void colorChanged(int color) {
				// TODO Auto-generated method stub
				textColor = color;
				text.setTextColor(textColor);
				colorPicker.setBackgroundColor(textColor);
				
			}
		}, textColor);
		colors.show();
	}
	
	private void initColorTable(final Dialog d) {
		GridView gw = (GridView)d.findViewById(R.id.picker);
		gw.setAdapter(new ColorPickerAdapter(d.getContext()));
		gw.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View v, int position,
					long arg3) {
				textColor = ColorPickerAdapter.backgrounds[position];
				text.setTextColor(textColor);
				colorPicker.setBackgroundColor(textColor);
				d.cancel();
			}
		});
	}
	
	private void setSaveButtonSettings(){
		saveButton.setOnClickListener(new  OnClickListener() {
			
			public void onClick(View v) {
				sendResult();
			}
		});
	}
	
	private void sendResult(){
		((CollageCreationActivity)getActivity()).addText(background, text.getText().toString(), textColor, textSize);
		getFragmentManager().popBackStack();
	}
	
	private EditText text;
	private int textColor;
	private int textSize;
	private int background = 0;
	ImageView colorPicker;
	ListView framesList;
	Spinner sizeSelector;
	ImageButton saveButton;
}
