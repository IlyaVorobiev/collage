package org.gtug.collage.fragments;

import org.gtug.collage.CollageCreationActivity;
import org.gtug.collage.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;

public class InterfaceFragment extends Fragment {
	
	public static InterfaceFragment newInstance(){
		InterfaceFragment instance = new InterfaceFragment();
		return instance;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setSaveButtonSettings();
		setTextButtonSettings();
		setImageButtonSettings();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.creation_interface, container, false);
		save = (ImageButton)v.findViewById(R.id.save);
		addText = (ImageButton)v.findViewById(R.id.text);
		addImage = (ImageButton)v.findViewById(R.id.image);
		return v;
	}
	
	public void setSaveButtonSettings(){
		save.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((CollageCreationActivity)getActivity()).saveCollage();
			}
		});
	}
	
	public void setTextButtonSettings(){
		addText.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((CollageCreationActivity)getActivity()).onInterfaceButtonClicked();
				((CollageCreationActivity)getActivity()).startTextEditFragment();
				
			}
		});
	}
	
	public void setImageButtonSettings(){
		addImage.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((CollageCreationActivity)getActivity()).onInterfaceButtonClicked();
				((CollageCreationActivity)getActivity()).choosePicture();
			}
		});
	}
	
	public void hide(){
		getFragmentManager().popBackStack();
	}
	
	ImageButton save;
	ImageButton addText;
	ImageButton addImage;
}
