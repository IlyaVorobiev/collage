package org.gtug.collage.fragments;

import org.gtug.classes.Collage;
import org.gtug.classes.ImageObject;
import org.gtug.classes.TextObject;
import org.gtug.collage.CollageCreationActivity;
import org.gtug.collage.R;
import org.gtug.ui.CollageLayout;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;


public class CollageCreationFragment extends Fragment {
	public static CollageCreationFragment newInstance(int background){
		CollageCreationFragment collageCreationFragment = new CollageCreationFragment();
		Bundle arguments = new Bundle();
		arguments.putInt("backgroundResource", background);
		collageCreationFragment.setArguments(arguments);
		return collageCreationFragment;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.creation, container, false);
		initLayout(v);
		return v;
	}
	
	protected void initLayout(View v){
		collage = new Collage(getArguments().getInt("backgroundResource"),(CollageCreationActivity)getActivity());
		initViews(v);
	}
	
	private void initViews(View v) {
		background = (CollageLayout)v.findViewById(R.id.background);
		if(collage.getBackground() == 0)
			background.setBackgroundColor(Color.WHITE);
		else
			background.setBackgroundResource(collage.getBackground());
		collage.setBg(background);
		
	}
	
	public void save() {
		collage.save();
	}
	
	public void choosePicture() {
		Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, 1);
	}
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.d("coollage","initViews");
		if(resultCode == getActivity().RESULT_OK){
			if(requestCode == 1) {
				addPicture(data);
			}
		} 
	}
	
	protected void addPicture(Intent data) {
		collage.width = background.getWidth();
		collage.height = background.getHeight();
		Uri chosenImageUri = data.getData();
		ImageObject picture = new ImageObject(0, 0, 0, (CollageCreationActivity)getActivity());
		picture.setParent(collage);
		picture.setSourceURI(chosenImageUri);
		picture.path = getPath(chosenImageUri);
		picture.show(background);
		picture.setDraggable();
		picture.setCoordsToView();
		collage.addObject(picture);
	}
	
	public String getPath(Uri uri) {
	    String[] projection = { MediaStore.Images.Media.DATA };
	    Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
	    int column_index = cursor
	            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	    cursor.moveToFirst();
	    return cursor.getString(column_index);
	}
	
	public void addText(Bundle data) {
		TextObject newText = new TextObject(0, 0, (CollageCreationActivity)getActivity(), data.getInt("bg", 0), data.getString("text"), data.getInt("textcolor", 0), data.getInt("textsize", 0));
		newText.setParent(collage);
		collage.addObject(newText);
		background.addView(newText.generateView(true));
	}
	
	public void hideInterface(){
		background.hideMenus();
	}
	
	private Collage collage;
	private CollageLayout background;
	RelativeLayout leftMenu;
	RelativeLayout rightMenu;
}
