package org.gtug.database;

import org.gtug.beans.DBCollage;
import org.gtug.beans.DBCollageContent;
import org.gtug.beans.DBCollageObject;
import org.gtug.beans.DBImageObject;
import org.gtug.beans.DBTextObject;

import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.util.Log;

public class DBTables {
	
	public static class CollageTable implements BaseColumns {
		public static String TABLE_NAME = "Collages";
		public static String BACKGROUND = "background";
		public static String WIDTH = "width";
		public static String HEIGHT = "height";
		
		public static final String SQL_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
				+ _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ BACKGROUND + " INTEGER,"
				+ WIDTH + " INTEGER,"
				+ HEIGHT + " INTEGER"
				+ ");";
		
		public static String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
		
		public static ContentValues makeContentValues(DBCollage collage) {
			ContentValues contentValues = new ContentValues();
			contentValues.put(BACKGROUND,collage.background);
			contentValues.put(WIDTH, collage.width);
			contentValues.put(HEIGHT, collage.height);
			return contentValues;
		}
		
		public static DBCollage getFromCursor(Cursor cursor) {
			Log.d("collage",Integer.toString(cursor.getInt(cursor.getColumnIndex(_ID))));
			final long id = cursor.getLong(cursor.getColumnIndex(_ID));
			final int background = cursor.getInt(cursor.getColumnIndex(BACKGROUND));
			final int width = cursor.getInt(cursor.getColumnIndex(WIDTH));
			final int height = cursor.getInt(cursor.getColumnIndex(HEIGHT));
			return new DBCollage(background,id,width,height);
		}
		
		public static String makeWhere(long id){
			return _ID + " = " + id;
		}
	}
	
	public static class CollagesContentTable implements BaseColumns{
		public static final String TABLE_NAME = "Collages_Content";
		public static final String COLLAGE_ID = "collage_id";
		public static final String OBJECT_ID = "object_id";
		
		public static final String SQL_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
				+ _ID + " LONG PRIMARY KEY,"
				+ COLLAGE_ID + " INTEGER,"
				+ OBJECT_ID + " INTEGER"
				+ ");";
		
		public static String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
		
		public static ContentValues makeContentValues(DBCollageContent collage) {
			ContentValues contentValues = new ContentValues();
			contentValues.put(COLLAGE_ID,collage.collageID);
			contentValues.put(OBJECT_ID, collage.objectID);
			return contentValues;
		}
		
		public static DBCollageContent getFromCursor(Cursor cursor) {
			final long id = cursor.getLong(cursor.getColumnIndex(_ID));
			final int collageID = cursor.getInt(cursor.getColumnIndex(COLLAGE_ID));
			final int objectID = cursor.getInt(cursor.getColumnIndex(OBJECT_ID));
			return new DBCollageContent(collageID, objectID);
		}
		
		public static String makeWhere(DBCollageContent collage){
			return COLLAGE_ID + " = " + collage.collageID + " AND " + OBJECT_ID + " = " + collage.objectID;
		}
		
		public static String makeWhere(long collageID) {
			return COLLAGE_ID + " = " + collageID;
		}
	}
	
	public static class ObjectsTable implements BaseColumns {
		public static final String TABLE_NAME = "Objects";
		public static final String COLLAGE_ID = "collage_id";
		public static final String TYPE = "type";
		public static final String OFFSET_TOP = "offset_top";
		public static final String OFFSET_LEFT = "offset_left";
		public static final String RESOURCE = "resource";
		
		public static final String SQL_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
				+ _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ COLLAGE_ID + " LONG,"
				+ TYPE + " TEXT,"
				+ OFFSET_LEFT + " INTEGER,"
				+ OFFSET_TOP + " INTEGER,"
				+ RESOURCE + " INTEGER"
				+ ");";
		
		public static String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
		
		public static ContentValues makeContentValues(DBCollageObject obj) {
			ContentValues contentValues = new ContentValues();
			contentValues.put(COLLAGE_ID,obj.collageID);
			contentValues.put(TYPE, obj.type);
			contentValues.put(OFFSET_LEFT, obj.offsetLeft);
			contentValues.put(OFFSET_TOP, obj.offsetTop);
			contentValues.put(RESOURCE, obj.resource);
			return contentValues;
		}
		
		public static DBCollageObject getFromCursor(Cursor cursor) {
			final long id = cursor.getLong(cursor.getColumnIndex(_ID));
			final int collageID = cursor.getInt(cursor.getColumnIndex(COLLAGE_ID));
			final String type = cursor.getString(cursor.getColumnIndex(TYPE));
			final int offsetLeft = cursor.getInt(cursor.getColumnIndex(OFFSET_LEFT));
			final int offsetTop = cursor.getInt(cursor.getColumnIndex(OFFSET_TOP));
			final int resource = cursor.getInt(cursor.getColumnIndex(RESOURCE));
			return new DBCollageObject(collageID,offsetLeft,offsetTop,resource,type);
		}
		
		public static String makeWhere(DBCollageObject object) {
			return COLLAGE_ID + " = " + object.collageID + " AND " + OFFSET_LEFT + " = " + object.offsetLeft 
									+ " AND " + OFFSET_TOP + " = " + object.offsetTop + " AND " + RESOURCE + " = " + object.resource; 
		}
		
		public static String makeWhere(long id){
			return _ID + " = " + id;
		}
		
		public static String makeWhere(int collage_id, int i) {
			return COLLAGE_ID + " = " + collage_id;
		}
	}
	
	public static class ImagesTable implements BaseColumns {
		public static final String TABLE_NAME = "Images";
		public static final String WIDTH = "width";
		public static final String HEIGHT = "height";
		public static final String PATH_TO_FILE = "path_to_file";
		public static final String SCALING_COEFF = "scaling_coeff";
		
		public static final String SQL_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
				+ _ID + " LONG PRIMARY KEY,"
				+ WIDTH + " INTEGER,"
				+ HEIGHT + " INTEGER," 
				+ PATH_TO_FILE + " TEXT,"
				+ SCALING_COEFF + " INTEGER"
				+ ");";
		
		public static String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
		
		public static ContentValues makeContentValues(DBImageObject img) {
			ContentValues contentValues = new ContentValues();
			contentValues.put(_ID,img.id);
			contentValues.put(WIDTH, img.width);
			contentValues.put(HEIGHT, img.height);
			contentValues.put(PATH_TO_FILE, img.path);
			contentValues.put(SCALING_COEFF, img.scalingCoeff);
			return contentValues;
		}
		
		public static DBImageObject getFromCursor(Cursor cursor) {
			final long id = cursor.getLong(cursor.getColumnIndex(_ID));
			final int width = cursor.getInt(cursor.getColumnIndex(WIDTH));
			final int height = cursor.getInt(cursor.getColumnIndex(HEIGHT));
			final String path = cursor.getString(cursor.getColumnIndex(PATH_TO_FILE));
			final int scalingCoeff = cursor.getInt(cursor.getColumnIndex(SCALING_COEFF));
			return new DBImageObject(width,height,path,id,scalingCoeff);
		}
		
		public static String makeWhere(long id) {
			return _ID + " = " + id;
		}
	}
	
	public static class TextTable implements BaseColumns {
		public static final String TABLE_NAME = "Text";
		public static final String TEXT = "text";
		public static final String COLOR = "color";
		public static final String SIZE = "size";
		
		public static final String SQL_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
				+ _ID + " LONG PRIMARY KEY,"
				+ TEXT + " TEXT,"
				+ COLOR + " INTEGER," 
				+ SIZE + " INTEGER"
				+ ");";
		
		public static String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
		
		public static ContentValues makeContentValues(DBTextObject txt) {
			ContentValues contentValues = new ContentValues();
			contentValues.put(_ID,txt.id);
			contentValues.put(TEXT, txt.text);
			contentValues.put(COLOR, txt.color);
			contentValues.put(SIZE, txt.size);
			return contentValues;
		}
		
		public static DBTextObject getFromCursor(Cursor cursor) {
			final long id = cursor.getLong(cursor.getColumnIndex(_ID));
			final String text = cursor.getString(cursor.getColumnIndex(TEXT));
			final int color = cursor.getInt(cursor.getColumnIndex(COLOR));
			final int size = cursor.getInt(cursor.getColumnIndex(SIZE));
			return new DBTextObject(id,text,color,size);
		}
		
		public static String makeWhere(long id) {
			return _ID + " = " + id;
		}
	}
}
