package org.gtug.database;

import java.util.ArrayList;

import org.gtug.beans.DBCollage;
import org.gtug.beans.DBCollageContent;
import org.gtug.beans.DBCollageObject;
import org.gtug.beans.DBImageObject;
import org.gtug.beans.DBTextObject;
import org.gtug.classes.Collage;
import org.gtug.classes.CollageObject;
import org.gtug.classes.ImageObject;
import org.gtug.classes.TextObject;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;

public class Provider {
	private static final String DATABASE_NAME = "CollageDB";
	private static final int DATABASE_VERSION = 1;
	
	
	public Provider(Context ctx) {
		super();
		this.ctx = ctx;
		dbHelper = new DBHelper(ctx, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	public long addColage(DBCollage collage) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		long id = 0;
		try{
			db.beginTransaction();
			id = db.insert(DBTables.CollageTable.TABLE_NAME, null, DBTables.CollageTable.makeContentValues(collage));
			db.setTransactionSuccessful();
		} catch(SQLException e) {
			throw e;
		} finally {
			db.endTransaction();
		}
		db.close();
		return id;
	}
	
	public void addCollageContent(DBCollageContent collage) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		try{
			db.beginTransaction();
			db.insert(DBTables.CollagesContentTable.TABLE_NAME, null, DBTables.CollagesContentTable.makeContentValues(collage));
				db.setTransactionSuccessful();
		} catch(SQLException e) {
			throw e;
		} finally {
			db.endTransaction();
		}
		db.close();
	}
	
	public long addObject(DBCollageObject object) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		long id = 0;
		try{
			db.beginTransaction();
			id = db.insert(DBTables.ObjectsTable.TABLE_NAME, null, DBTables.ObjectsTable.makeContentValues(object));
			db.setTransactionSuccessful();
		} catch(SQLException e) {
			throw e;
		} finally {
			db.endTransaction();
		}
		db.close();
		
		return id;
	}
	
	public void addImageObject(DBImageObject object) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		try{
			db.beginTransaction();
			db.insert(DBTables.ImagesTable.TABLE_NAME, null, DBTables.ImagesTable.makeContentValues(object));
			db.setTransactionSuccessful();
		} catch(SQLException e) {
			throw e;
		} finally {
			db.endTransaction();
		}
		db.close();
	}
	
	public void addTextObject(DBTextObject object) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		try{
			db.beginTransaction();
			db.insert(DBTables.TextTable.TABLE_NAME, null, DBTables.TextTable.makeContentValues(object));
			db.setTransactionSuccessful();
		} catch(SQLException e) {
			throw e;
		} finally {
			db.endTransaction();
		}
		db.close();
	}
	
	public ArrayList<Collage> getAllColages() {
		ArrayList<Collage> allCollages = new ArrayList<Collage>();
		ArrayList<DBCollage>allCollageBeans = getAllCollageBeans();
		for(DBCollage collageBean:allCollageBeans) {
			Collage collage = new Collage(collageBean.background,ctx);
			collage.id = collageBean.id;
			ArrayList<DBCollageContent> allCollageContentBeans = getCollageContentById(collage.id);
			
			for(DBCollageContent collageContent: allCollageContentBeans) {
				
				DBCollageObject collageObjectBean = getCollageObjectById(collageContent.objectID);
				CollageObject collageObject;
				if(collageObjectBean.type.equals("image")) {
					DBImageObject imageSettings = getImageObjectById(collageContent.objectID);
					collageObject = new ImageObject(collageObjectBean.offsetLeft, collageObjectBean.offsetTop, collageObjectBean.resource, ctx);
					((ImageObject)collageObject).path = imageSettings.path;
					((ImageObject)collageObject).setHeight(imageSettings.height);
					((ImageObject)collageObject).setWidth(imageSettings.width);
					((ImageObject)collageObject).setScalingCoeff(imageSettings.scalingCoeff);
					((ImageObject)collageObject).setParent(collage);
				} else {
					
					DBTextObject textSettings = getTextObjectById(collageContent.objectID);
					collageObject = new TextObject(collageObjectBean.offsetLeft, collageObjectBean.offsetTop, ctx, collageObjectBean.resource, textSettings.text, textSettings.color, textSettings.size);
				}
				collage.addObject(collageObject);
			}
			
			allCollages.add(collage);
		}
		return allCollages;
	}
	
	private ArrayList<DBCollage> getAllCollageBeans() {
		Log.d("collage","yes");
		final SQLiteDatabase db = dbHelper.getReadableDatabase();
		ArrayList<DBCollage> allCollageBeans = new ArrayList<DBCollage>();
		Cursor cursor = db.query(DBTables.CollageTable.TABLE_NAME, null,null, null, null, null, null);
		if(cursor.moveToFirst()){
			do {
				allCollageBeans.add(DBTables.CollageTable.getFromCursor(cursor));
			} while(cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return allCollageBeans;
	}
	
	private ArrayList<DBCollageContent> getCollageContentById(long id){
		final SQLiteDatabase db = dbHelper.getReadableDatabase();
		ArrayList<DBCollageContent> allCollageContentBeans = new ArrayList<DBCollageContent>();
		Cursor cursor = db.query(DBTables.CollagesContentTable.TABLE_NAME, null, DBTables.CollagesContentTable.makeWhere(id), null, null, null, null);
		if(cursor.moveToFirst()){
			do {
				allCollageContentBeans.add(DBTables.CollagesContentTable.getFromCursor(cursor));
			} while(cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return allCollageContentBeans;
	}
	
	private DBCollageObject getCollageObjectById(long id) {
		final SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor cursor = db.query(DBTables.ObjectsTable.TABLE_NAME, null, DBTables.ObjectsTable.makeWhere(id), null, null, null, null);
		DBCollageObject co;
		if(cursor.moveToFirst()){
			co = DBTables.ObjectsTable.getFromCursor(cursor);
		} else {
			co = null;
		}
		cursor.close();
		db.close();
		return co;
	}
	
	private DBImageObject getImageObjectById(long id) {
		final SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor cursor = db.query(DBTables.ImagesTable.TABLE_NAME, null, DBTables.ImagesTable.makeWhere(id), null, null, null, null);
		DBImageObject imgobj;
		if(cursor.moveToFirst())
			imgobj = DBTables.ImagesTable.getFromCursor(cursor);
		else imgobj = null;
		cursor.close();
		db.close();
		return imgobj;
	}
	
	private DBTextObject getTextObjectById(long id) {
		final SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor cursor = db.query(DBTables.TextTable.TABLE_NAME, null, DBTables.TextTable.makeWhere(id), null, null, null, null);
		DBTextObject txtobj;
		if(cursor.moveToFirst())
			txtobj = DBTables.TextTable.getFromCursor(cursor);
		else txtobj = null;
		cursor.close();
		db.close();
		return txtobj;
	}
	
	public void removeCollage(long id){
		ArrayList<DBCollageContent> allCollageContentBeans = getCollageContentById(id);
		for(DBCollageContent collageContent: allCollageContentBeans){
			DBCollageObject collageObjectBean = getCollageObjectById(collageContent.objectID);
			CollageObject collageObject;
			final SQLiteDatabase db = dbHelper.getWritableDatabase();
			
			if(collageObjectBean.type.equals("image")) {
				db.delete(DBTables.ImagesTable.TABLE_NAME, DBTables.ImagesTable.makeWhere(collageContent.objectID), null);
			} else {
				db.delete(DBTables.TextTable.TABLE_NAME, DBTables.TextTable.makeWhere(collageContent.objectID), null);
			}
			db.delete(DBTables.ObjectsTable.TABLE_NAME, DBTables.ObjectsTable.makeWhere(collageContent.objectID), null);
			db.close();
		}
		final SQLiteDatabase db = dbHelper.getWritableDatabase();
		
		db.delete(DBTables.CollageTable.TABLE_NAME, DBTables.CollageTable.makeWhere(id), null);
		db.close();
		
	}
	
	private static class DBHelper extends SQLiteOpenHelper {

		public DBHelper(Context context, String name, CursorFactory factory,
				int version) {
			super(context, name, factory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			Log.d("collage","Creating");
			db.execSQL(DBTables.CollageTable.SQL_CREATE_TABLE);
			db.execSQL(DBTables.CollagesContentTable.SQL_CREATE_TABLE);
			db.execSQL(DBTables.ObjectsTable.SQL_CREATE_TABLE);
			db.execSQL(DBTables.ImagesTable.SQL_CREATE_TABLE);
			db.execSQL(DBTables.TextTable.SQL_CREATE_TABLE);
			
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.d("collage","upgrading");
			if (newVersion != DATABASE_VERSION) {
				throw new RuntimeException("cannot upgrade to " + newVersion + ". it must be " + DATABASE_VERSION);
			}
			
			db.execSQL(DBTables.CollageTable.SQL_DROP_TABLE);
			db.execSQL(DBTables.CollagesContentTable.SQL_DROP_TABLE);
			db.execSQL(DBTables.ObjectsTable.SQL_DROP_TABLE);
			db.execSQL(DBTables.ImagesTable.SQL_DROP_TABLE);
			db.execSQL(DBTables.TextTable.SQL_DROP_TABLE);
			onCreate(db);
			
		}
		
	}
	
	private Context ctx;
	private DBHelper dbHelper;
	
}
