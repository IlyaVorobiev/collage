package org.gtug.beans;

public class DBCollageObject extends Bean{
	public final long collageID;
	public final String type;
	public final int offsetLeft;
	public final int offsetTop;
	public final int resource;
	
	public DBCollageObject(long id, int offsetLeft,
			int offsetTop, int resource, String type) {
		super();
		this.collageID = id;
		this.offsetLeft = offsetLeft;
		this.offsetTop = offsetTop;
		this.resource = resource;
		this.type = type;
	}
	
}
