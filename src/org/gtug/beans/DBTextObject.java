package org.gtug.beans;

public class DBTextObject extends Bean{
	public final long id;
	public final String text;
	public final int color;
	public final int size;
	
	public DBTextObject(long id, String text, int color, int size) {
		super();
		this.id = id;
		this.text = text;
		this.color = color;
		this.size = size;
	}

}
