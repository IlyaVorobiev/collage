package org.gtug.beans;

public class DBCollage {
	public long id;
	public int background;
	public int width;
	public int height;

	public DBCollage(int background,int width,int height) {
		super();
		this.background = background;
		this.width = width;
		this.height = height;
	}
	
	public DBCollage(int background, long id, int width, int height){
		super();
		this.background = background;
		this.id = id;
		this.width = width;
		this.height = height;
	}
}
