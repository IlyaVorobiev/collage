package org.gtug.beans;

public class DBCollageContent {
	public final long collageID;
	public final long objectID;
	
	public DBCollageContent(long id, long objId) {
		super();
		this.collageID = id;
		this.objectID = objId;
		
	}
}
