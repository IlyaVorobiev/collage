package org.gtug.beans;

public class DBImageObject extends Bean{
	public final long id;
	public final int width;
	public final int height;
	public final String path;
	public final int scalingCoeff;
	
	public DBImageObject(int width, int height, String path, long id, int scaclingCoeff) {
		super();
		this.width = width;
		this.height = height;
		this.path = path;
		this.id = id;
		this.scalingCoeff = scaclingCoeff;
	}
}
