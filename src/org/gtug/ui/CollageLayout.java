package org.gtug.ui;

import org.gtug.collage.CollageCreationActivity;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

public class CollageLayout extends RelativeLayout {
	
	private RelativeLayout leftMenu;
	private RelativeLayout rightMenu;
	private boolean areMenusVisible = true;
	private boolean wasClick = false;
	public CollageLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	

	public CollageLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}



	public CollageLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public void setMenus(RelativeLayout lm, RelativeLayout rm) {
		leftMenu = lm;
		rightMenu = rm;
	}
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		// TODO Auto-generated method stub
		super.onInterceptTouchEvent(ev);
		
		switch(ev.getAction()){
		case MotionEvent.ACTION_DOWN:
			wasClick = true;
			break;
		case MotionEvent.ACTION_MOVE:
			wasClick = false;
			break;
		case MotionEvent.ACTION_UP:
			Log.d("collage","Action up");
			if(wasClick){
				if(areMenusVisible) {
					hideMenus();
				} else {
					showMenus();
				}
			}
			break;
		}
			
		return false;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		switch(event.getAction()){
		
		case MotionEvent.ACTION_UP:
			Log.d("collage","Action up");
			if(wasClick){
				if(areMenusVisible) {
					hideMenus();
				} else {
					showMenus();
				}
			}
			break;
		}
		return true;
	}



	public void hideMenus(){
		((CollageCreationActivity)getContext()).hideInterface();
		areMenusVisible = false;
	}
	
	public void showMenus(){
		((CollageCreationActivity)getContext()).showInterface();
		areMenusVisible = true;
	}
	
}
