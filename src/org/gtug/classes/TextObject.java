package org.gtug.classes;

import org.gtug.beans.Bean;
import org.gtug.beans.DBCollageContent;
import org.gtug.beans.DBCollageObject;
import org.gtug.beans.DBTextObject;
import org.gtug.database.Provider;

import android.content.Context;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TextObject extends CollageObject {

	public TextObject(int x, int y, Context ctx, int resource, String text, int textColor, int textSize) {
		super(x, y, resource, ctx);
		this.text = text;
		this.textColor = textColor;
		this.textSize = textSize;
	}

	public void setParent(Collage parent) {
		this.parent = parent;
	}
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getTextColor() {
		return textColor;
	}

	public void setTextColor(int textColor) {
		this.textColor = textColor;
	}

	public int getTextSize() {
		return textSize;
	}

	public void setTextSize(int textSize) {
		this.textSize = textSize;
	}

	@Override
	public View generateView(Boolean isDraggable) {
	    view = new TextView(ctx);
		view.setBackgroundResource(resource);
		view.setText(text);
		view.setTextColor(textColor);
		view.setTextSize(textSize);
		view.setGravity(Gravity.CENTER);
		if(isDraggable)
			setDraggable();
		return view;
	}
	
	private void setDraggable() {
		view.setOnTouchListener(new OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				switch(event.getAction()){
				case MotionEvent.ACTION_DOWN:{
					offsetX = (int) event.getX();
					offsetY = (int) event.getY();
					parent.changeZIndexOf(TextObject.this);
					parent.getBg().bringChildToFront(view);
					break;
				}
				case MotionEvent.ACTION_MOVE:{
					x = (int) event.getRawX() - offsetX;
					y = (int) event.getRawY() - offsetY;
					setCoordsToView();
					break;
				}
				}
				return true;
			}
		});
	}
	
	private void setCoordsToView() {
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
		params.leftMargin = x;
		params.topMargin = y;
		params.rightMargin = -x;
		params.bottomMargin = -y;
		view.setLayoutParams(params);
	}
	
	@Override
	public void moveToPoint(int x, int y) {
		this.x = x;
		this.y = y;
		setCoordsToView();
	}

	@Override
	public Bean save(long id, Provider p, String type) {
		DBCollageObject collageObject = (DBCollageObject)super.save(id,p,"text");
		long objId = p.addObject(collageObject);
		p.addCollageContent(new DBCollageContent(id, objId));
		p.addTextObject(makeBean(objId));
		return null;
	}

	private DBTextObject makeBean(long id) {
		return new DBTextObject(id, text, textColor, textSize);
	}
	
	@Override
	public void show(RelativeLayout parent) {
		super.show(parent);
		generateView(false);
		parent.addView(view);
		setCoordsToView();
	}

	private int offsetX = 0;
	private int offsetY = 0;
	Collage parent;
	private String text;
	private int textColor;
	private int textSize;
	private TextView view;
}
