package org.gtug.classes;

import java.util.ArrayList;

import org.gtug.beans.DBCollage;
import org.gtug.database.Provider;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
//git commit
public class Collage {
	public Collage(int bg, Context ctx){
		super();
		allObjects = new ArrayList<CollageObject>();
		background = bg;
		mProvider = new Provider(ctx);
	}
	
	public void setBg(RelativeLayout bg) {
		this.bg = bg;
		Log.d("collage","Display on start  - " + Integer.toString(bg.getWidth()));
	}
	
	public RelativeLayout getBg() {
		return bg;
	}
	
	public void addObject(CollageObject obj) {
		allObjects.add(obj);
	}
	
	public void changeZIndexOf(CollageObject obj) {
		allObjects.remove(obj);
		allObjects.add(obj);
	}
	
	public int getBackground() {
		return background;
	}

	public void setBackground(int background) {
		this.background = background;
	}

	public void save() {
		id = mProvider.addColage(makeBean());
		for(CollageObject object:allObjects) {
			object.save(id,mProvider,"");
		}
	}

	private DBCollage makeBean(){
		return new DBCollage(background,width,height);
	}
	
	public void generateBG(Context ctx) {
		bg = new RelativeLayout(ctx);
		if(background == 0){
			bg.setBackgroundColor(Color.WHITE);
		} else
			bg.setBackgroundResource(background);
	}
	
	public void setSizes() {
		bg.setLayoutParams(new LayoutParams(-1,-1));
	}
	
	public void show() {
		if(background == 0){
			bg.setBackgroundColor(Color.WHITE);
		} else
			bg.setBackgroundResource(background);
		for(CollageObject collageObject: allObjects) {
			collageObject.show(bg);
		}
	}
	
	public void stop() {
		for(CollageObject collageObject: allObjects) {
//			collageObject.stop();
		}
	}
	
	private int background;
	private RelativeLayout bg;
	public int width;
	public int height;
	private ArrayList<CollageObject> allObjects;
	private Provider mProvider;
	public long id;
}
