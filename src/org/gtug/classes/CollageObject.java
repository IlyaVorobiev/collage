package org.gtug.classes;

import org.gtug.beans.Bean;
import org.gtug.beans.DBCollageObject;
import org.gtug.database.Provider;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;

public class CollageObject {
	
	public CollageObject(int x, int y, int resource, Context ctx) {
		super();
		this.x = x;
		this.y = y;
		this.resource = resource;
		this.ctx = ctx;
	}
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getResource() {
		return resource;
	}
	public void setResource(int resource) {
		this.resource = resource;
	}

	public View generateView(Boolean isDraggable) {
		return null;
	}
	
	public void moveToPoint(int x, int y) {
		
	}
	
	public Bean save(long id, Provider p, String type) {
		return new DBCollageObject(id, x, y, resource, type);
	}
	
	public void show(RelativeLayout parent) {
		
	}
	
	public void stop(){
		
	}
	
	protected int x;
	protected int y;
	protected int resource;
	protected Context ctx;
}
