package org.gtug.adapters;

import java.util.ArrayList;

import org.gtug.classes.Collage;

import android.content.Context;
import android.graphics.Color;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CollageViewAdapter extends PagerAdapter {

	 @Override
     public int getCount() {
             return allCollages.size();
     }

 @Override
	public int getItemPosition(Object object) {
		// TODO Auto-generated method stub
		return POSITION_NONE;
	}

/**
  * Create the page for the given position.  The adapter is responsible
  * for adding the view to the container given here, although it only
  * must ensure this is done by the time it returns from
  * {@link #finishUpdate()}.
  *
  * @param container The containing View in which the page will be shown.
  * @param position The page position to be instantiated.
  * @return Returns an Object representing the new page.  This does not
  * need to be a View, but can be some other container of the page.
  */
     @Override
     public Object instantiateItem(View collection, int position) {
    	int i = allCollages.size() - position -1;
    	allCollages.get(i).generateBG(ctx);
    	allCollages.get(i).setSizes();
    	((ViewPager) collection).addView(allCollages.get(i).getBg());
    	allCollages.get(i).width = collection.getWidth();
    	allCollages.get(i).height = collection.getHeight();
		allCollages.get(i).show();
    	return allCollages.get(i).getBg();
     }

 /**
  * Remove a page for the given position.  The adapter is responsible
  * for removing the view from its container, although it only must ensure
  * this is done by the time it returns from {@link #finishUpdate()}.
  *
  * @param container The containing View from which the page will be removed.
  * @param position The page position to be removed.
  * @param object The same object that was returned by
  * {@link #instantiateItem(View, int)}.
  */
     @Override
     public void destroyItem(View collection, int position, Object view) {
    	 	int i = allCollages.size() - position -1;
    	 	allCollages.get(i).stop();
             ((ViewPager) collection).removeView((RelativeLayout) view);
     }

     
     
     @Override
     public boolean isViewFromObject(View view, Object object) {
             return view==((RelativeLayout)object);
     }

     
 /**
  * Called when the a change in the shown pages has been completed.  At this
  * point you must ensure that all of the pages have actually been added or
  * removed from the container as appropriate.
  * @param container The containing View which is displaying this adapter's
  * page views.
  */
     @Override
     public void finishUpdate(View arg0) {}
     

     @Override
     public void restoreState(Parcelable arg0, ClassLoader arg1) {}

     @Override
     public Parcelable saveState() {
             return null;
     }

     @Override
     public void startUpdate(View arg0) {}
	public CollageViewAdapter(ArrayList<Collage> allCollages, Context ctx) {
		super();
		this.allCollages = allCollages;
		this.ctx = ctx;
	}
//
//	@Override
//	public int getCount() {
//		// TODO Auto-generated method stub
//		return allCollages.size();
//	}
//
//	@Override
//	public boolean isViewFromObject(View arg0, Object arg1) {
//		// TODO Auto-generated method stub
//		return false;
//	}
//	
//	@Override
//	public Object instantiateItem(ViewGroup container, int position) {
//		super.instantiateItem(container, position);
////		allCollages.get(position).generateBG(ctx);
////		allCollages.get(position).show();
////		((ViewPager) container).addView(allCollages.get(position).getBg());
//		 TextView tv = new TextView(ctx);
//         tv.setText("Bonjour PAUG " + position);
//         tv.setTextColor(Color.WHITE);
//         tv.setTextSize(30);
//         
//         ((ViewPager) container).addView(tv,0);
//         
//         return tv;
////		return allCollages.get(position).getBg();
//	}
//
//	@Override
//	public void destroyItem(ViewGroup container, int position, Object object) {
//		// TODO Auto-generated method stub
//		super.destroyItem(container, position, object);
//	}
//
	public ArrayList<Collage> allCollages;
	private Context ctx;
}
