package org.gtug.adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SizePickerAdapter extends BaseAdapter {

	public SizePickerAdapter(Context ctx) {
		super();
		this.ctx = ctx;
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return sizes.length;
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		TextView size;
		if(convertView == null){
			size = new TextView(ctx);
		} else{
			size = (TextView)convertView;
		}
		size.setText(Integer.toString(sizes[position]));
		size.setTextColor(Color.BLACK);
		return size;
	}

	public static int[] sizes = {12,16,18,24,32,48,72,96};
	private Context ctx;
}
