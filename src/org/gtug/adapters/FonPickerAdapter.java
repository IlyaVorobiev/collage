package org.gtug.adapters;

import org.gtug.collage.R;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class FonPickerAdapter extends BaseAdapter {
	
	public FonPickerAdapter(Context ctx) {
		super();
		this.ctx = ctx;
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return backgrounds.length;
	}

	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	public View getView(int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		ImageView bgItem;
		if(view == null){
			bgItem = new ImageView(ctx);
		} else {
			bgItem = (ImageView)view;
		}
		//TODO: replace background color to image resource
		if(backgrounds[position].thumbnail == 0){
			bgItem.setBackgroundColor(Color.WHITE);
		} else
			bgItem.setBackgroundResource(backgrounds[position].thumbnail);
		bgItem.setLayoutParams(new LayoutParams(200, 200));
		return bgItem;
	}
	
	public class Background{
		public int thumbnail;
		public int fullSize;
		public Background(int thumbnail, int fullSize) {
			super();
			this.thumbnail = thumbnail;
			this.fullSize = fullSize;
		}
		
		
	}
	
	public Background[] backgrounds = {new Background(0,0),new Background(R.drawable.greenwp_small, R.drawable.greenwp_big), new Background(R.drawable.projectors_small, R.drawable.projectors_big),
										new Background(R.drawable.blackboard_small, R.drawable.blackboard_big)
										};
	private Context ctx;
}
