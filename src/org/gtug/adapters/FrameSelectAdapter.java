package org.gtug.adapters;

import org.gtug.collage.R;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class FrameSelectAdapter extends BaseAdapter {

	
	
	public FrameSelectAdapter(Context ctx) {
		super();
		this.ctx = ctx;
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return frames.length;
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView pict;
		if(convertView == null) {
			pict = new ImageView(ctx);
		} else {
			pict = (ImageView)convertView;
		}
		pict.setImageResource(frames[position].thumbnail);
		return pict;
	}
	
	public class Frame {
		public int thumbnail;
		public int picture;
		
		public Frame(int thumbnail,int picture) {
			this.thumbnail = thumbnail;
			this.picture = picture;
		}
	}
	public Frame[] frames = {new Frame(R.drawable.no, 0),new Frame(R.drawable.cloud_text_small, R.drawable.cloud_text_big),new Frame(R.drawable.fluffy_small, R.drawable.fluffy_big)};
	private Context ctx;
}
