package org.gtug.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ColorPickerAdapter extends BaseAdapter {
	
	public ColorPickerAdapter(Context ctx) {
		super();
		this.ctx = ctx;
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return backgrounds.length;
	}

	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	public View getView(int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		ImageView bgItem;
		if(view == null){
			bgItem = new ImageView(ctx);
		} else {
			bgItem = (ImageView)view;
		}
		//TODO: replace background color to image resource
		bgItem.setBackgroundColor(backgrounds[position]);
		bgItem.setLayoutParams(new LayoutParams(50, 50));
		return bgItem;
	}
	
	public static int[] backgrounds = {Color.BLACK,Color.DKGRAY,Color.GRAY,
										Color.BLUE,Color.MAGENTA,Color.RED,
										Color.GREEN,Color.CYAN,Color.WHITE
										};
	private Context ctx;
}
